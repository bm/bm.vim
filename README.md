# Installation

## Installer vim-plug

[Installer vim-plug](https://github.com/junegunn/vim-plug)

## Télécharger l'extension

```bash
cd ~/.config/nvim/plugged/ # ou vim, c'est selon
git clone https://git.bitmycode.com/bm/bm.vim.git
```

## Charger l'extension

Dans `~/.config/nvim/init.vim`, rajouter le plugin :

```vim
Plug '~/.config/nvim/plugged/bm.vim'
```

