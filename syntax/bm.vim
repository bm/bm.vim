" Vim syntax file
" Language:         BootMonkey
" Maintainer:       Yannick A.
" Last Change:      2018 January 20
" Original Author:  Yannick A

if exists("b:current_syntax")
  finish
endif

syn case match


" Constants
syn region  bmString          start=+"+ skip=+\\"+ end=+"+ contains=bmEscape,bmInterpolation
syn match   bmEscape          contained "\\x\x\{2}"
syn match   bmEscape          contained "\\u\x\{4}"
syn match   bmEscape          contained "\\U\x\{6}"
syn match   bmEscape          contained "\\[\\abefnrtv\"'0]"
syn match   bmInterpolation   contained "${.\{-\}}"

hi def link bmString          String
hi def link bmEscape          Character
hi def link bmInterpolation   Keyword

syn match   bmInt             "\d\+"
syn match   bmInt             "0b[01]\+"
syn match   bmInt             "0o[0-7]\+"
syn match   bmInt             "0x\x\+"
syn match   bmCharError       "'"
syn match   bmChar            "'[^\\']'"
syn match   bmChar            "'\\[\\abefnrtv\"'0]'" contains=bmEscape
syn match   bmChar            "'\\x\x\{2}'" contains=bmEscape
syn match   bmChar            "'\\u\x\{4}'" contains=bmEscape
syn match   bmChar            "'\\U\x\{6}'" contains=bmEscape
syn keyword bmBoolean         true false
syn match   bmReal            "\d\+\%(\.\d\+\)\=[Ee][+-]\=\d\+"
syn match   bmReal            "\d\+\.\d\+\%([Ee][+-]\=\d\+\)\="

hi def link bmInt             Number
hi def link bmChar            Number
hi def link bmBoolean         Boolean
hi def link bmReal            Float
hi def link bmCharError       Error
hi def link bmIntError        Error


" Identifiers
syn match   bmId              "\<\a[a-zA-Z0-9_']*" nextgroup=bmCap,bmCapMod
syn match   bmPrivateId       "\<_[a-zA-Z0-9_']\+"
syn cluster bmIdentifier      contains=bmId,bmPrivateId
"hi def link bmId              Identifier
"hi def link bmPrivateId       Identifier

syn keyword bmBuiltinType     Void Bool Num Char String Symbol List Set Dict Tuple Func
syn keyword bmBuiltinType     Int UInt Int8 UInt8 Int16 UInt16 Int32 UInt32 Int64 UInt64 Int128 UInt128
syn keyword bmBuiltinType     Float Float32 Float64 Float96 Float128
hi def link bmBuiltinType     Type

syn region  bmMethodDecl      matchgroup=bmMethodKeyword start=+\<\%(fun\|be\|new\)\>+ end=+[[({]\@=+ contains=bmCap,bmMethod,@bmComment
syn keyword bmMethodKeyword   contained fun be new
syn match   bmMethod          contained "\<\a[a-zA-Z0-9_']*"
syn match   bmMethod          contained "\<_[a-zA-Z0-9_']\+"
hi def link bmMethod          Function
hi def link bmMethodKeyword   Keyword

syn region  bmVarDecl         matchgroup=bmVarKeyword start=+\<\%(var\|let\|embed\)\>+ end=+[:=]\@=+ contains=bmVar,@bmComment
syn keyword bmVarKeyword      contained var let embed
syn match   bmVar             contained "\<\a[a-zA-Z0-9_']*"
syn match   bmVar             contained "\<_[a-zA-Z0-9_']\+"
hi def link bmVar             Identifier
hi def link bmVarKeyword      Keyword


" Operators and delimiters
syn match   bmCapModError     +[\^\!]+
hi def link bmCapModError     Error

syn match   bmQuestion        +?+
hi def link bmQuestion        StorageClass

syn match   bmAt              +@+
hi def link bmAt              Delimiter

syn match   bmAtOpError       +@[^ 	\-[("a-zA-Z_]+
syn match   bmAtIdError       +@\s\+[^"a-zA-Z_]+
hi def link bmAtIdError       Error
hi def link bmAtOpError       Error

syn keyword bmOp1             and or xor is isnt not consume
syn match   bmOp2             +\([=!]=\|[<>]=\|<<\|>>\|@-\|[-+<>*/%&|]\)+
hi def link bmOp1             Keyword
hi def link bmOp2             Keyword


" Keywords
syn keyword bmConditional       if else
syn keyword bmLoop              loop while for do
syn keyword bmStatement         return break continue
syn keyword bmKeyword           class in var by to till
syn keyword bmMatch             match
syn keyword bmPackage           use as
hi def link bmConditional       Keyword 
hi def link bmLoop              Keyword 
hi def link bmStatement         Keyword 
hi def link bmKeyword           Keyword
hi def link bmMatch             Keyword
hi def link bmPackage           Include

syn match bmOperator            +[+|-|*|/|%|**]=?+
hi def link bmOperator          Operator


" Parentheses
syn match   bmParenError      +[()]+
syn region  bmParen           transparent start=+(+ end=+)+ contains=TOP,bmParenError
syn match   bmArrayError      +[\[\]]+
syn region  bmArray           transparent start=+\[+ end=+]+ contains=TOP,bmArrayError
syn match   bmConstError      +[{}]+
syn region  bmConst           transparent start=+{+ end=+}+ contains=TOP,bmConstError

hi def link bmParenError      Error
hi def link bmArrayError      Error
hi def link bmConstError      Error


" Methods
syn match   bmIntroducer      +=>+
hi def link bmIntroducer      Delimiter


" Comments
syn region bmLineComment      start=+#+ end=+$+ contains=bmTodo keepend
syn region bmNestedComment    start=+/\*+ end=+\*/+ contains=bmTodo,bmNestedComment
syn cluster bmComment         contains=bmLineComment,bmNestedComment
syn keyword bmTodo            contained TODO FIXME XXX

hi def link bmLineComment     Comment
hi def link bmNestedComment   Comment
hi def link bmTodo            Todo

let b:current_syntax = "bm"

